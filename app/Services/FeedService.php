<?php

namespace App\Services;

use Illuminate\Support\Collection;
use SimplePie;

class FeedService
{
    /**
     * @return Collection<int, \SimplePie_Item>
     */
    public function get(): Collection
    {
        $pie = new SimplePie();
        $pie->enable_cache(false);
        $pie->enable_order_by_date();
        $pie->set_feed_url([
            'https://feeds.feedburner.com/tweakers',
            'https://feeds.feedburner.com/nosnieuwsbinnenland',
            'https://feeds.feedburner.com/nosnieuwsbuitenland',
            'https://feeds.feedburner.com/nosnieuwstech',
            'https://feeds.feedburner.com/nossportformule1',
            'https://feeds.feedburner.com/nossportalgemeen',
            'https://feeds.feedburner.com/nosnieuwsalgemeen',
            'https://www.nu.nl/rss/Algemeen',
        ]);
        $pie->init();
        return collect($pie->get_items(0, 15))
            ->unique(fn (\SimplePie_Item $item) => $item->get_id());
    }
}
