<?php

namespace App\Http\Livewire\Widget;

use App\Services\FeedService;
use Carbon\CarbonInterval;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;

class NewsFeed extends Component
{
    public function render(FeedService $service): View
    {
        $items = Cache::remember('news', CarbonInterval::make(5, 'minute'),
            fn () => $service->get()
        );
        return view('livewire.widget.news-feed', [
            'items' => $items,
        ]);
    }
}
