<?php

namespace App\Http\Livewire\Widget;

use App\Services\WeatherService;
use Carbon\CarbonInterval;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;

class Weather extends Component
{
    public function render(WeatherService $service): View
    {
        $data = Cache::remember('weather', CarbonInterval::make(180, 'seconds'),
            fn () => $service->get()
        );

        return view('livewire.widget.weather', [
            'data' => $data,
        ]);
    }
}
