<div class="flex flex-col gap-4 lg:overflow-y-hidden">
    {{ $slot }}
</div>
