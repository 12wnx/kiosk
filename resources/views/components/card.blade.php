<div {{ $attributes->class("p-4 rounded shadow min-h-0 bg-gray-200 overflow-y-hidden") }}>
    {{ $slot }}
</div>
