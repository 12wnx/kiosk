<x-card>
    <x-card-title class="mb-3">@lang('Agenda')</x-card-title>
    <p class="text-xl text-blue-700 mb-3">{{ str(now()->isoFormat('dddd D MMMM YYYY, HH:mm'))->ucfirst() }}</p>
    <ul class="flex flex-col gap-3">
        @foreach($agenda as $name => $date)
            <li class="pb-2 flex flex-row justify-between border-b border-gray-400">
                <span>{{ $name }}</span>
                <span>{{ $date }}</span>
            </li>
        @endforeach
    </ul>
</x-card>
