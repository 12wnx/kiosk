<x-card wire:poll.visible.30s="">
    <x-card-title>@lang('Weer')</x-card-title>
    <p class="mb-3 text-gray-500 text-sm">
        @lang('bijgewerkt om :time', ['time' => now()->isoFormat('HH:mm')])
    </p>
    <div class="flex-grow flex flex-row gap-8 items-center">
        <div class="pl-4">
            @if($icon = data_get($data, 'weather.0.icon'))
                <img
                    class="object-scale-down w-32 h-32 rounded-full shadow bg-white"
                    alt="weather icon"
                    src="{{ sprintf('https://openweathermap.org/img/wn/%s@4x.png', $icon) }}"
                >
            @else
                <svg class="object-scale-down h-32 w-32 p-4 rounded-full shadow bg-white"
                     xmlns="http://www.w3.org/2000/svg"
                     width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
                    <path
                        d="M8 11a3 3 0 1 1 0-6 3 3 0 0 1 0 6zm0 1a4 4 0 1 0 0-8 4 4 0 0 0 0 8zM8 0a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 0zm0 13a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 13zm8-5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2a.5.5 0 0 1 .5.5zM3 8a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2A.5.5 0 0 1 3 8zm10.657-5.657a.5.5 0 0 1 0 .707l-1.414 1.415a.5.5 0 1 1-.707-.708l1.414-1.414a.5.5 0 0 1 .707 0zm-9.193 9.193a.5.5 0 0 1 0 .707L3.05 13.657a.5.5 0 0 1-.707-.707l1.414-1.414a.5.5 0 0 1 .707 0zm9.193 2.121a.5.5 0 0 1-.707 0l-1.414-1.414a.5.5 0 0 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .707zM4.464 4.465a.5.5 0 0 1-.707 0L2.343 3.05a.5.5 0 1 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .708z"/>
                </svg>
            @endif
        </div>
        <div class="flex-grow">
            <h3 class="text-4xl font-semibold">
                {{ number_format(data_get($data, 'main.temp'), 1) }}&deg;C
            </h3>
            <p class="text-xl">{{ data_get($data, 'weather.0.description') }}</p>
        </div>
    </div>
</x-card>
