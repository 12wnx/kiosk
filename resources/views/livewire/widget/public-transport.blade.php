<x-card wire:poll.visible.300s="">
    <x-card-title>@lang('Openbaar Vervoer')</x-card-title>
    <p class="mb-3 text-gray-500 text-sm">
        @lang('bijgewerkt om :time', ['time' => now()->isoFormat('HH:mm')])
    </p>
    <ul class="flex flex-col gap-3">
        @foreach($transport as $stop => $departures)
            <li>
                <h3 class="text-lg font-semibold mb-2">{{ $stop }}</h3>
                <ul class="flex flex-col gap-2">
                    @foreach($departures as $departure => $in)
                        <li class="pb-2 flex flex-row justify-between border-b border-gray-400">
                            <span>{{ $departure }}</span>
                            <span>{{ $in }}</span>
                        </li>
                    @endforeach
                </ul>
            </li>
        @endforeach
    </ul>
</x-card>
